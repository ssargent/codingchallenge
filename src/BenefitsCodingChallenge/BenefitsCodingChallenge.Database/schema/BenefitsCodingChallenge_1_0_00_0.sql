set nocount on

declare @currentVersion nvarchar(128)
declare @schemaName nvarchar(64)
declare @installPatch int

set @schemaName = 'BENEFITSCODINGCHALLENGE_SCHEMA'

select @currentVersion = VersionIdentifier
from SchemaVersion 
where SchemaName = @schemaName and IsCurrentVersion	= 1

exec @installPatch = usp_dba_Schema @Update=@schemaName, @Version='0.0.0.0', @With='1.0.0.0'
if(@installPatch = 1)
begin
	begin try
		begin transaction 
		
			create table Benefits_ConfigSettings
			(
				ID uniqueidentifier not null rowguidcol default(newsequentialid()),
				Name nvarchar(64) not null,
				DataType nvarchar(64) not null,
				Value nvarchar(64) not null,
				constraint pk_configsettings_id primary key (ID)
			)

			create table Benefits_Packages 
			(
				ID uniqueidentifier not null rowguidcol default(newsequentialid()),
				Name nvarchar(64) not null,
				SalaryPerPeriod money not null,
				EmployeeBenefitsCost money not null,
				DependentBenefitsCost money not null,
				IsDefault bit not null,
				constraint pk_Packages_id primary key (ID)
			)

			create table Benefits_Employees
			(
				ID uniqueidentifier not null rowguidcol default(newsequentialid()),
				FirstName nvarchar(64) not null,
				LastName nvarchar(64) not null,
				StartDate datetime not null default(getutcdate()),
				BenefitsPackageID uniqueidentifier not null,
				constraint pk_Employees_id primary key (ID),
				constraint fk_Employees_Benefits foreign key (BenefitsPackageID) references Benefits_Packages (ID)
			)

			create table Benefits_Dependents
			(
				ID uniqueidentifier not null rowguidcol default(newsequentialid()),
				QualifiedEmployeeID uniqueidentifier not null,
				FirstName nvarchar(64) not null,
				LastName nvarchar(64) not null,
				constraint pk_Dependents_id primary key (ID),
				constraint fk_Dependents_Employees foreign key (QualifiedEmployeeID) references Benefits_Employees (ID)
			)

			exec usp_dba_SetCurrentSchema @schemaName, '1.0.0.0'
		commit transaction
	end try
	begin catch
		rollback transaction
	
		exec usp_RethrowError
	end catch
end
 
 set nocount off
 
 GO

 
