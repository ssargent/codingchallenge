set nocount on

declare @currentVersion nvarchar(128)
declare @schemaName nvarchar(64)
declare @installPatch int

set @schemaName = 'BENEFITSCODINGCHALLENGE_SCHEMA'

select @currentVersion = VersionIdentifier
from SchemaVersion 
where SchemaName = @schemaName and IsCurrentVersion	= 1

exec @installPatch = usp_dba_Schema @Update=@schemaName, @Version='1.0.0.0', @With='1.0.1.0'
if(@installPatch = 1)
begin
	begin try
		begin transaction 
		
			alter table Benefits_ConfigSettings add IsDeleted bit not null default(0);
			alter table Benefits_Dependents add IsDeleted bit not null default(0);
			alter table Benefits_Employees add IsDeleted bit not null default(0);
			alter table Benefits_Packages add IsDeleted bit not null default(0);

			exec usp_dba_SetCurrentSchema @schemaName, '1.0.1.0'
		commit transaction
	end try
	begin catch
		rollback transaction
	
		exec usp_RethrowError
	end catch
end
 
 set nocount off
 
 GO

 
