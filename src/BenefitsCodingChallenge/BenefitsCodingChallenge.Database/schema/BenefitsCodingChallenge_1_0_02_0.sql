set nocount on

declare @currentVersion nvarchar(128)
declare @schemaName nvarchar(64)
declare @installPatch int

set @schemaName = 'BENEFITSCODINGCHALLENGE_SCHEMA'

select @currentVersion = VersionIdentifier
from SchemaVersion 
where SchemaName = @schemaName and IsCurrentVersion	= 1

exec @installPatch = usp_dba_Schema @Update=@schemaName, @Version='1.0.1.0', @With='1.0.2.0'
if(@installPatch = 1)
begin
	begin try
		begin transaction 
		
			declare @one as uniqueidentifier
			declare @two as uniqueidentifier
			declare @three as uniqueidentifier

			set @one = cast(cast(1 as binary(8)) as uniqueidentifier)
			set @two = cast(cast(2 as binary(8)) as uniqueidentifier)
			set @three = cast(cast(3 as binary(8)) as uniqueidentifier)

			insert into Benefits_Packages (ID, Name, SalaryPerPeriod, EmployeeBenefitsCost, DependentBenefitsCost, IsDefault, IsDeleted)
			values
			(@one, 'Standard', 2000.00, 1000.00 / 26, 500.00 / 26, 1, 0)

			insert into Benefits_Packages (ID, Name, SalaryPerPeriod, EmployeeBenefitsCost, DependentBenefitsCost, IsDefault, IsDeleted)
			values
			(@two, 'Premium', 3000.00, 1200.00 / 26, 600.00 / 26, 0, 0)

			insert into Benefits_Packages (ID, Name, SalaryPerPeriod, EmployeeBenefitsCost, DependentBenefitsCost, IsDefault, IsDeleted)
			values
			(@three, 'Ultimate', 4000.00, 1400.00 / 26, 700.00 / 26, 0, 0)

			 
			exec usp_dba_SetCurrentSchema @schemaName, '1.0.2.0'
		commit transaction
	end try
	begin catch
		rollback transaction
	
		exec usp_RethrowError
	end catch
end
 
 set nocount off
 
 GO

 
