﻿#region Using

using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BenefitsCodingChallenge.Interfaces;

#endregion

namespace BenefitsCodingChallenge.WebApp.Controllers
{
    public class ReportsController : Controller
    {
        public ReportsController(IPaymentServiceAsync paymentServiceAsync)
        {
            PaymentService = paymentServiceAsync;
        }

        protected IPaymentServiceAsync PaymentService { get; set; }

        // GET: Reports
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> PaymentSummary()
        {
            var payments = await PaymentService
                .CalculatePayments(DateTime.UtcNow, DateTime.UtcNow.AddYears(1), true);

            return View("PaymentSummary", payments.OrderBy(p => p.Employee.ID));
        }

        public async Task<ActionResult> PaymentSummaryWithNoDiscounts()
        {
            var payments = await PaymentService
                .CalculatePayments(DateTime.UtcNow, DateTime.UtcNow.AddYears(1), false);

            return View("PaymentSummary", payments.OrderBy(p => p.Employee.ID));
        }

        public async Task<ActionResult> PaymentsByEmployee()
        {
            var payments = await PaymentService
                .CalculatePayments(DateTime.UtcNow, DateTime.UtcNow.AddYears(1), true);

            return View("PaymentsByEmployee", payments.OrderBy(p => p.Employee.ID));
        }

        public async Task<ActionResult> PaymentsByEmployeeNoDiscounts()
        {
            var payments = await PaymentService
                .CalculatePayments(DateTime.UtcNow, DateTime.UtcNow.AddYears(1), false);

            return View("PaymentsByEmployee", payments.OrderBy(p => p.Employee.ID));
        }

        public async Task<ActionResult> Payments()
        {
            var payments = await PaymentService
                .CalculatePayments(DateTime.UtcNow, DateTime.UtcNow.AddYears(1), true);

            return View("PaymentSchedule", payments.OrderBy(p => p.PaymentDate));
        }

        public async Task<ActionResult> PaymentsWithNoDiscounts()
        {
            var payments = await PaymentService
                .CalculatePayments(DateTime.UtcNow, DateTime.UtcNow.AddYears(1), false);

            return View("PaymentSchedule", payments.OrderBy(p => p.PaymentDate));
        }
    }
}