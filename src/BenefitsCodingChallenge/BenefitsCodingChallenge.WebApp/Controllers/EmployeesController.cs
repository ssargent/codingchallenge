﻿#region Using

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using BenefitsCodingChallenge.Interfaces;
using BenefitsCodingChallenge.WebApp.Models;
using Omu.ValueInjecter;
using WebGrease.Css.Extensions;

#endregion

namespace BenefitsCodingChallenge.WebApp.Controllers
{
    public class EmployeesController : Controller
    {
        public EmployeesController(IEmployeeServiceAsync employeeService, IBenefitsServiceAsync benefitsService)
        {
            EmployeeService = employeeService;
            BenefitsService = benefitsService;
        }

        protected IEmployeeServiceAsync EmployeeService { get; set; }
        protected IBenefitsServiceAsync BenefitsService { get; set; }

        // GET: Employee
        public async Task<ActionResult> Index()
        {
            var employees =
                await EmployeeService.GetEmployees(e => e.IsDeleted == false, "Dependents", "BenefitsPackage");
            var employeesList = new List<Models.Employee>();
            // This all should be MUCH Cleaner...
            employees.ForEach(e =>
            {
                var ne = new Models.Employee();
                ne.InjectFrom(e);
                ne.BenefitsPackage = new Models.Package();
                ne.BenefitsPackage.InjectFrom(e.BenefitsPackage);
                ne.Dependents = new List<Dependent>();

                e.Dependents.ForEach(d =>
                {
                    var nd = new Dependent();
                    nd.InjectFrom(d);
                    ne.Dependents.Add(nd);
                });

                employeesList.Add(ne);
            });

            employeesList.InjectFrom(employees);

            return View("Index", employeesList);
        }

        // GET: Employee/Details/5
        public ActionResult Details(Guid id)
        {
            return View();
        }

        // GET: Employee/Create
        public async Task<ActionResult> Create()
        {
            var employeeViewModel = new EmployeeViewModel();
            var packages = await BenefitsService.GetPackages();

            employeeViewModel.StartDate = DateTime.Now.Date;

            packages.ForEach(p => { employeeViewModel.AvailablePackages.Add(p.ID, p.Name); });

            return View(employeeViewModel);
        }

        // POST: Employee/Create
        [HttpPost]
        public async Task<ActionResult> Create(EmployeeViewModel employeeModel)
        {
            try
            {
                var employee = new Entities.Employee()
                {
                    FirstName = employeeModel.FirstName,
                    LastName = employeeModel.LastName,
                    StartDate = employeeModel.StartDate
                };

                var dependents = employeeModel.ListOfDependents.Split(',');
                if (dependents.Any())
                    employee.Dependents = new List<Entities.Dependent>();

                dependents.ForEach(d =>
                {
                    if (d.Trim().Contains(' '))
                    {
                        var name = d.Trim().Split(' ');
                        employee.Dependents.Add(new Entities.Dependent()
                        {
                            FirstName = name[0],
                            LastName = name[1],
                            QualifiedEmployee = employee
                        });
                    }
                    else
                    {
                        employee.Dependents.Add(new Entities.Dependent()
                        {
                            FirstName = d.Trim(),
                            LastName = employee.LastName,
                            QualifiedEmployee = employee
                        });
                    }
                });

                employee.BenefitsPackageID = employeeModel.SelectedBenefitPackageID;

                employee = await EmployeeService.AddEmployee(employee);

                return RedirectToAction("index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Employee/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Employee/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Employee/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}