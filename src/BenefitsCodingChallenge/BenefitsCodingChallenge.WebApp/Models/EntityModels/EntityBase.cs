﻿#region Using

using System;

#endregion

namespace BenefitsCodingChallenge.WebApp.Models
{
    public abstract class EntityBase
    {
        public virtual Guid ID { get; set; }
        public virtual bool IsDeleted { get; set; }
    }
}