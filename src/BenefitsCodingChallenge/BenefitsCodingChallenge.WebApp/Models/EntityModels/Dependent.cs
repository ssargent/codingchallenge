#region Using

using System;

#endregion

namespace BenefitsCodingChallenge.WebApp.Models
{
    public class Dependent : EntityBase
    {
        public Guid QualifiedEmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual Employee QualifiedEmployee { get; set; }
    }
}