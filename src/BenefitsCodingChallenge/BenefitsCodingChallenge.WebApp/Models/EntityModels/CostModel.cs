﻿#region Using

using System;

#endregion

namespace BenefitsCodingChallenge.WebApp.Models
{
    public class CostModel
    {
        public DateTime PayCheckDate { get; set; }
        public Decimal PaidToEmployee { get; set; }
        public Decimal BenefitsCost { get; set; }
    }
}