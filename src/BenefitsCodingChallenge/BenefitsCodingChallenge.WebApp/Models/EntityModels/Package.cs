#region Using

using System.Collections.Generic;

#endregion

namespace BenefitsCodingChallenge.WebApp.Models
{
    public class Package : EntityBase
    {
        public string Name { get; set; }
        public decimal SalaryPerPeriod { get; set; }
        public decimal EmployeeBenefitsCost { get; set; }
        public decimal DependentBenefitsCost { get; set; }
        public bool IsDefault { get; set; }

        public virtual IList<Employee> Employees { get; set; }
    }
}