﻿#region Using

using System;
using System.Collections.Generic;
using System.ComponentModel;

#endregion

namespace BenefitsCodingChallenge.WebApp.Models
{
    public class EmployeeViewModel
    {
        public EmployeeViewModel()
        {
            AvailablePackages = new Dictionary<Guid, string>();
        }

        public Guid ID { get; set; }

        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        public string LastName { get; set; }

        [DisplayName("Start Date")]
        public DateTime StartDate { get; set; }

        public Guid SelectedBenefitPackageID { get; set; }

        [DisplayName("List of Dependents")]
        public string ListOfDependents { get; set; }

        public Dictionary<Guid, string> AvailablePackages { get; set; }
    }
}