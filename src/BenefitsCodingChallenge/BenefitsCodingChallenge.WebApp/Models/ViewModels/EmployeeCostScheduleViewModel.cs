﻿#region Using

using System.Collections.Generic;

#endregion

namespace BenefitsCodingChallenge.WebApp.Models
{
    public class EmployeeCostScheduleViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<string> Dependents { get; set; }
        public List<CostModel> ScheduleOfCosts { get; set; }
    }
}