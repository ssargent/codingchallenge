﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BenefitsCodingChallenge.Entities
{
    public class Payment
    {
        public Payment()
        {
            Costs = new List<CostComponent>();
        }
        public Employee Employee { get; set; }
        public Decimal Salary { get; set; }
        public Decimal Benefits { get; set; }
        public Decimal TakeHome { get; set; }
        public List<CostComponent> Costs { get; set; }
        public DateTime PaymentDate { get; set; }

        public bool IsDiscounted
        {
            get
            {
                var applied = false;

                foreach (var cost in Costs)
                {
                    if (cost.DiscountsApplied.Any())
                    {
                        applied = true;
                        break;
                    }
                }

                return applied;
            }
        }
    }

    public class CostComponent
    {
        public CostComponent()
        {
            DiscountsApplied = new List<string>();
        }
        public Employee Employee { get; set; }
        public Dependent Dependent { get; set; }
        public Decimal Amount { get; set; }
        public List<String> DiscountsApplied { get; set; } 
    }

}
