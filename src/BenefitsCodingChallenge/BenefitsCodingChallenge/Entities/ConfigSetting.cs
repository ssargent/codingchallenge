﻿#region Using



#endregion

namespace BenefitsCodingChallenge.Entities
{
    public class ConfigSetting : EntityBase
    {
        public string Name { get; set; }
        public string DataType { get; set; }
        public string Value { get; set; }
    }
}