﻿using System;

namespace BenefitsCodingChallenge.Entities
{
    public abstract class EntityBase
    {
        public virtual Guid ID { get; set; }
        public virtual bool IsDeleted { get; set; }
    }
}