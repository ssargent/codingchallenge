﻿using System;

namespace BenefitsCodingChallenge.Entities
{
    public class Dependent : EntityBase
    {
        public Guid QualifiedEmployeeID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual Employee QualifiedEmployee { get; set; }
    }
}