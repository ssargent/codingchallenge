﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace BenefitsCodingChallenge.Entities
{
    public class Employee : EntityBase
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime StartDate { get; set; }
        public Guid BenefitsPackageID { get; set; }

        public virtual IList<Dependent> Dependents { get; set; }
        public virtual Package BenefitsPackage { get; set; }
    }
}