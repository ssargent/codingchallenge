﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using BenefitsCodingChallenge.Data;
using BenefitsCodingChallenge.Entities;
using BenefitsCodingChallenge.Interfaces;
using BenefitsCodingChallenge.Repositories;
using BenefitsCodingChallenge.Services;

namespace BenefitsCodingChallenge
{
    public class CodingChallengeModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            base.Load(builder);

            // Configure repositories
            builder.RegisterType<EntityRepositoryAsync<ConfigSetting>>().As<IRepositoryAsync<ConfigSetting>>();
            builder.RegisterType<EntityRepositoryAsync<Dependent>>().As<IRepositoryAsync<Dependent>>();
            builder.RegisterType<EntityRepositoryAsync<Employee>>().As<IRepositoryAsync<Employee>>();
            builder.RegisterType<EntityRepositoryAsync<Package>>().As<IRepositoryAsync<Package>>();

            // Configure Services
            builder.RegisterType<BenefitsServiceAsync>().As<IBenefitsServiceAsync>();
            builder.RegisterType<EmployeeServiceAsync>().As<IEmployeeServiceAsync>();
            builder.RegisterType<PaymentServiceAsync>().As<IPaymentServiceAsync>();
            builder.RegisterType<DiscountServiceAsync>().As<IDiscountServiceAsync>();

            // Configure Database
            builder.RegisterType<BenefitsDatabase>().As<BenefitsDatabase>();

        }
    }
}
