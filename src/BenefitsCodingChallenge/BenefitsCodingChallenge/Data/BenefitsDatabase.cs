﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BenefitsCodingChallenge.Data.Configuration;
using BenefitsCodingChallenge.Entities;

namespace BenefitsCodingChallenge.Data
{
    public class BenefitsDatabase : DbContext
    {
        public BenefitsDatabase()
        {
            AutoSave = true;
            IncludeSoftDeletedData = false;
        }
        public DbSet<ConfigSetting> ConfigSettings { get; set; }
        public DbSet<Dependent> Dependents { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Package> Packages { get; set; }
        public bool AutoSave { get; set; }
        public bool IncludeSoftDeletedData { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.Add(new ConfigSettingConfiguration());
            modelBuilder.Configurations.Add(new DependentConfiguration());
            modelBuilder.Configurations.Add(new EmployeeConfiguration());
            modelBuilder.Configurations.Add(new PackageConfiguration());
        }
    }

}
