using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using BenefitsCodingChallenge.Entities;

namespace BenefitsCodingChallenge.Data.Configuration
{
    public class EmployeeConfiguration : EntityTypeConfiguration<Employee>
    {
        public EmployeeConfiguration()
        {
            ToTable("Benefits_Employees")
                .HasKey(e => e.ID);
            Property(e => e.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            HasRequired(e => e.BenefitsPackage)
                .WithMany(p => p.Employees)
                .HasForeignKey(e => e.BenefitsPackageID);
        }
    }
}