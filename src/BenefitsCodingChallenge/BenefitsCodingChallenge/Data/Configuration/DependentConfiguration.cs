using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using BenefitsCodingChallenge.Entities;

namespace BenefitsCodingChallenge.Data.Configuration
{
    public class DependentConfiguration : EntityTypeConfiguration<Dependent>
    {
        public DependentConfiguration()
        {
            ToTable("Benefits_Dependents")
                .HasKey(d => d.ID);
            Property(d => d.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            HasRequired(d => d.QualifiedEmployee)
                .WithMany(e => e.Dependents)
                .HasForeignKey(d => d.QualifiedEmployeeID);
        }
    }
}