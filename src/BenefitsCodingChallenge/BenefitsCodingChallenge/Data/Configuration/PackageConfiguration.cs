using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using BenefitsCodingChallenge.Entities;

namespace BenefitsCodingChallenge.Data.Configuration
{
    public class PackageConfiguration : EntityTypeConfiguration<Package>
    {
        public PackageConfiguration()
        {
            ToTable("Benefits_Packages")
                .HasKey(p => p.ID);
            Property(p => p.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}