﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BenefitsCodingChallenge.Entities;

namespace BenefitsCodingChallenge.Data.Configuration
{
    public class ConfigSettingConfiguration : EntityTypeConfiguration<ConfigSetting>
    {
        public ConfigSettingConfiguration()
        {
            ToTable("Benefits_ConfigSettings")
                .HasKey(c => c.ID);
            Property(c => c.ID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
