﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BenefitsCodingChallenge.Entities;

namespace BenefitsCodingChallenge.Interfaces
{
    public interface IDiscount
    {
        DateTime StartDate { get; set; } 
        DateTime EndDate { get; set; }

        Task<bool> ApplyDiscount(List<Payment> payments);
    }
}