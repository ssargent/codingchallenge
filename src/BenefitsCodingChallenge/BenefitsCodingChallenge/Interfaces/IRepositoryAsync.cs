﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BenefitsCodingChallenge.Entities;

namespace BenefitsCodingChallenge.Interfaces
{
    public interface IRepositoryAsync<T> where T : EntityBase
    {
        Task<T> GetByID(Guid id);
        Task<T> Create(T entity);
        Task<T> Update(T entity);
        Task Delete(T entity, bool hard = false);

        Task<IEnumerable<T>> Get(Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "",
            bool asReadOnly = true);
    }
}