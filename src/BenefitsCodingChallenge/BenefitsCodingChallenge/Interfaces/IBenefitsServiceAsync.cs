﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BenefitsCodingChallenge.Entities;

namespace BenefitsCodingChallenge.Interfaces
{
    public interface IBenefitsServiceAsync
    {
        Task<List<Package>> GetPackages();
    }

    public interface IDiscountServiceAsync
    {
        Task ApplyDiscounts(List<Payment> payments);
    }
}