﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BenefitsCodingChallenge.Entities;

namespace BenefitsCodingChallenge.Interfaces
{
    public interface IEmployeeServiceAsync
    {
        Task<Employee> AddEmployee(Employee employee);
        Task<List<Employee>> GetEmployees(Expression<Func<Employee, bool>> filter, params string[] include);
    }
}