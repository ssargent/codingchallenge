﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BenefitsCodingChallenge.Entities;

namespace BenefitsCodingChallenge.Interfaces
{
    public interface IPaymentServiceAsync
    {
        Task<List<Payment>> CalculatePayments(DateTime startDate, DateTime endDate, bool applyDiscounts);

    }
}