﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using BenefitsCodingChallenge.Data;
using BenefitsCodingChallenge.Entities;
using BenefitsCodingChallenge.Interfaces;

namespace BenefitsCodingChallenge.Repositories
{
    public class EntityRepositoryAsync<T> : IRepositoryAsync<T> where T : EntityBase
    {
        public EntityRepositoryAsync(BenefitsDatabase database)
        {
            Database = database;
        } 
        protected BenefitsDatabase Database { get; set; }
        public async Task<T> Create(T entity)
        {
            var record = Database.Set<T>().Add(entity);
            await SaveAsRequired();
            return record;
        }

        private async Task SaveAsRequired()
        {
            if (Database.AutoSave)
               await Database.SaveChangesAsync();
        }

        public async Task Delete(T entity, bool hard = false)
        {
            if (hard)
            {
                if (Database.Entry(entity).State == EntityState.Deleted)
                {
                    Database.Set<T>().Attach(entity);
                }
                Database.Set<T>().Remove(entity);

                await SaveAsRequired();
            }
            else
            {
                await Update(entity);
            }
        }

        public async Task<IEnumerable<T>> Get(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, string includeProperties = "", bool asReadOnly = true)
        {
            IQueryable<T> query = Database.Set<T>();
            var resultData = default(List<T>);
            if (asReadOnly)
                query = query.AsNoTracking();
            if (filter != null)
            {
                query = query.Where(filter);
                if (!Database.IncludeSoftDeletedData)
                    query = query.Where(_ => _.IsDeleted == false);
            }
            query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
            .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));
            if (orderBy != null)
            {
                resultData = await orderBy(query).ToListAsync();
            }
            else
            {
                resultData = await query.ToListAsync();
            }

            return resultData;
        }

        public async Task<T> GetByID(Guid id)
        {
            var entity = await Database.Set<T>().FindAsync(id);
            if (entity == null || (entity.IsDeleted && Database.IncludeSoftDeletedData == false))
                throw new BenefitsRecordNotFoundException();
            return entity;
        }

        public async Task<T> Update(T entity)
        {
            var record = Database.Set<T>().Attach(entity);
            Database.Entry(entity).State = EntityState.Modified;
            await SaveAsRequired();
            return record;
        }

        public async Task SaveChanges()
        {
            await Database.SaveChangesAsync();
        }
    }
}
