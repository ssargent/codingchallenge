﻿using System;
using System.Runtime.Serialization;

namespace BenefitsCodingChallenge.Repositories
{
    [Serializable]
    public class BenefitsRecordNotFoundException : Exception
    {
        public BenefitsRecordNotFoundException()
        {
        }

        public BenefitsRecordNotFoundException(string message) : base(message)
        {
        }

        public BenefitsRecordNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected BenefitsRecordNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}