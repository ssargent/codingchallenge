﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BenefitsCodingChallenge.Entities;
using BenefitsCodingChallenge.Interfaces;

namespace BenefitsCodingChallenge.Services
{
    public class BenefitsServiceAsync : IBenefitsServiceAsync
    {
        protected  IRepositoryAsync<Package> PackageRepository { get; set; } 
        public BenefitsServiceAsync(IRepositoryAsync<Package> packageRepository)
        {
            PackageRepository = packageRepository;
        }
        public async Task<List<Package>> GetPackages()
        {
            var packages = await PackageRepository.Get();
            return packages.ToList();
        }
    }
}