﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BenefitsCodingChallenge.Entities;
using BenefitsCodingChallenge.Interfaces;

namespace BenefitsCodingChallenge.Services
{
    public class PaymentServiceAsync : IPaymentServiceAsync
    {
        protected IEmployeeServiceAsync EmployeeService { get; set; }
        protected IDiscountServiceAsync DiscountService { get; set; }

        public PaymentServiceAsync(IEmployeeServiceAsync employeeServiceAsync, IDiscountServiceAsync discountServiceAsync)
        {
            EmployeeService = employeeServiceAsync;
            DiscountService = discountServiceAsync;
        }

        public async  Task<List<Payment>> CalculatePayments(DateTime startDate, DateTime endDate, bool applydiscounts)
        {
            const int DAYS_IN_YEAR = 365;
            const int TIME_BETWEEN_PAYPERIODS = DAYS_IN_YEAR/26;

            var listOfPayments = new List<Payment>();
            var startOfYear = new DateTime(startDate.Year, 1, 1);
            var firstPaymentDate = GetFirstPayment(startOfYear, startDate, TIME_BETWEEN_PAYPERIODS);
            var employeeList = await EmployeeService.GetEmployees(e => e.StartDate < endDate, "Dependents", "BenefitsPackage");

            employeeList.ForEach(e =>
            {
                listOfPayments.AddRange(GeneratePayments(e, firstPaymentDate, endDate, TIME_BETWEEN_PAYPERIODS));
            });

            if(applydiscounts)
                await DiscountService.ApplyDiscounts(listOfPayments);

            CalculateTotals(listOfPayments);

            return listOfPayments;
        }

        private void CalculateTotals(List<Payment> listOfPayments)
        {
              listOfPayments.ForEach(p =>
              {
                  p.Benefits = p.Costs.Select(c => c.Amount).Sum();
                  p.TakeHome = p.Salary - p.Benefits;
              });
        }

        private List<Payment> GeneratePayments(Employee employee, DateTime firstPaymentDate, DateTime endDate, int payperiod)
        {
            var paymentsList = new List<Payment>();
            var paymentdate = firstPaymentDate;

            while (paymentdate < endDate)
            {
                var payment = new Payment()
                {
                    Employee = employee,
                    Salary = employee.BenefitsPackage.SalaryPerPeriod,
                    PaymentDate = paymentdate
                };

                payment.Costs.Add( new CostComponent()
                {
                    Employee =  employee,
                    Amount = employee.BenefitsPackage.EmployeeBenefitsCost
                });

                employee.Dependents.ToList().ForEach(d =>
                {
                    payment.Costs.Add( new CostComponent()
                    {
                        Employee = employee,
                        Dependent =  d,
                        Amount = employee.BenefitsPackage.DependentBenefitsCost
                    });
                });

                paymentsList.Add(payment);
                paymentdate = paymentdate.AddDays(payperiod);
            }

            return paymentsList;
        }

        private DateTime GetFirstPayment(DateTime yearStart, DateTime startDate, int payperiod)
        {
            var testDate = yearStart;

            while (testDate.DayOfWeek != DayOfWeek.Friday)
                testDate = testDate.AddDays(1);

            while (testDate < startDate)
                testDate = testDate.AddDays(payperiod);
                
            return testDate;
        }
    }
}