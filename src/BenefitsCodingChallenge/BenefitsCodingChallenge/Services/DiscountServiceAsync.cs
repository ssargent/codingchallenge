using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BenefitsCodingChallenge.Discounts;
using BenefitsCodingChallenge.Entities;
using BenefitsCodingChallenge.Interfaces;

namespace BenefitsCodingChallenge.Services
{
    public class DiscountServiceAsync : IDiscountServiceAsync
    {
        public DiscountServiceAsync()
        {
            Discounts = new List<IDiscount>();
            Discounts.Add(new NameStartsWithADiscount()); // this should come from a database, or reflected code.
        }
        protected List<IDiscount> Discounts { get; set; } 
        public async Task ApplyDiscounts(List<Payment> payments)
        {
            Discounts.ForEach(d =>
            {
                d.ApplyDiscount(payments);
            });
        }
    }

}