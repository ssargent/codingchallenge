﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using BenefitsCodingChallenge.Entities;
using BenefitsCodingChallenge.Interfaces;

namespace BenefitsCodingChallenge.Services
{
    public class EmployeeServiceAsync : IEmployeeServiceAsync
    {
        protected IRepositoryAsync<Employee> EmployeeRepository { get; set; }
        protected IRepositoryAsync<Dependent> DependentRepository { get; set; }

        public EmployeeServiceAsync(IRepositoryAsync<Employee> employeeRepository,
            IRepositoryAsync<Dependent> dependentRepository)
        {
            EmployeeRepository = employeeRepository;
            DependentRepository = dependentRepository;
        }
        public async Task<Employee> AddEmployee(Employee employee)
        {
            try
            {
                return await EmployeeRepository.Create(employee);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<Employee>> GetEmployees(Expression<Func<Employee, bool>> filter, params string[] include)
        {
            var employees = await EmployeeRepository.Get(filter, null, String.Join(",", include));
            return employees.ToList();
        }
    }
}