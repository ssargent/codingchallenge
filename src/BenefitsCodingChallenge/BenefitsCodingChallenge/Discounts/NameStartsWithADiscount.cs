﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BenefitsCodingChallenge.Entities;
using BenefitsCodingChallenge.Interfaces;

namespace BenefitsCodingChallenge.Discounts
{
    public class NameStartsWithADiscount : IDiscount
    {
        public NameStartsWithADiscount()
        {
            StartDate = DateTime.MinValue;
            EndDate = DateTime.MaxValue;
        }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public async Task<bool> ApplyDiscount(List<Payment> payments)
        {
            payments.ForEach(p =>
            {
                p.Costs.ForEach(c =>
                {
                    // use the employee
                    if (c.Dependent == null)
                    {
                        if (c.Employee.FirstName.ToLower().StartsWith("a"))
                        {
                            c.Amount = c.Amount - c.Amount*.1m;
                            c.DiscountsApplied.Add(this.GetType().Name);
                        }
                    }
                    else
                    {
                        if (c.Dependent.FirstName.ToLower().StartsWith("a"))
                        {
                            c.Amount = c.Amount - c.Amount * .1m;
                            c.DiscountsApplied.Add(this.GetType().Name);
                        }
                    }
                });
            });

            return true;
        }
    }
}
